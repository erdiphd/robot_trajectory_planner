# CMake generated Testfile for 
# Source directory: /home/ubuntu/kmr-planning/src
# Build directory: /home/ubuntu/kmr-planning/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(robot_motion_planning)
subdirs(robot_motion_execution/execution_msgs)
subdirs(robot_motion_execution/execution_param_config)
subdirs(planner_data_structures)
subdirs(planner_param_config)
subdirs(planner_statistics)
subdirs(planning_heuristics)
subdirs(robot_motion_execution/trajectory_planning)
subdirs(kuka_motion_control)
subdirs(planning_world_builder)
subdirs(robot_motion_execution/eband_local_planner)
subdirs(kmr_kuka_iiwa)
subdirs(robot_motion_execution/motion_trajectory_execution)
subdirs(robot_motion_execution/execution_server)
subdirs(moveit_test)
subdirs(validity_checker)
subdirs(birrt_star_algorithm)
subdirs(rrt_star_algorithm)
subdirs(planning_scenarios)
subdirs(robot_description_pkg)
subdirs(omnirob_lbr_sdh_moveit_config)
