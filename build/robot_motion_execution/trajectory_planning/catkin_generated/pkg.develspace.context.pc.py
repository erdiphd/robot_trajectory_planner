# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/ubuntu/kmr-planning/src/robot_motion_execution/trajectory_planning/include".split(';') if "/home/ubuntu/kmr-planning/src/robot_motion_execution/trajectory_planning/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-ltrajectory_planning".split(';') if "-ltrajectory_planning" != "" else []
PROJECT_NAME = "trajectory_planning"
PROJECT_SPACE_DIR = "/home/ubuntu/kmr-planning/devel"
PROJECT_VERSION = "0.0.0"
