# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include;/opt/ros/kinetic/share/orocos_kdl/../../include;/usr/include/eigen3".split(';') if "${prefix}/include;/opt/ros/kinetic/share/orocos_kdl/../../include;/usr/include/eigen3" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;std_msgs;kdl_parser;moveit_core;moveit_ros_planning".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lkuka_motion_control;/opt/ros/kinetic/lib/liborocos-kdl.so.1.3.2".split(';') if "-lkuka_motion_control;/opt/ros/kinetic/lib/liborocos-kdl.so.1.3.2" != "" else []
PROJECT_NAME = "kuka_motion_control"
PROJECT_SPACE_DIR = "/home/ubuntu/kmr-planning/install"
PROJECT_VERSION = "0.0.0"
