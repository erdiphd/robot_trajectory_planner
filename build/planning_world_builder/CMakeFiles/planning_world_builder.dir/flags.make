# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.5

# compile CXX with /usr/bin/c++
CXX_FLAGS = -fPIC   -std=c++11

CXX_DEFINES = -DROSCONSOLE_BACKEND_LOG4CXX -DROS_BUILD_SHARED_LIBS=1 -DROS_PACKAGE_NAME=\"planning_world_builder\" -D__cplusplus=201103L -Dplanning_world_builder_EXPORTS

CXX_INCLUDES = -I/home/ubuntu/kmr-planning/src/planning_world_builder/include -I/home/ubuntu/kmr-planning/src/kuka_motion_control/include -I/opt/ros/kinetic/include -I/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp -I/opt/ros/kinetic/share/orocos_kdl/../../include -I/usr/include/eigen3 

