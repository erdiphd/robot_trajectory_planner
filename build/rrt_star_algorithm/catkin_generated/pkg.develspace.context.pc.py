# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/ubuntu/kmr-planning/src/rrt_star_algorithm/include".split(';') if "/home/ubuntu/kmr-planning/src/rrt_star_algorithm/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;std_msgs;kuka_motion_control;moveit_ros_planning_interface;planning_world_builder;planning_heuristics;planner_data_structures;validity_checker".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lrrt_star_algorithm".split(';') if "-lrrt_star_algorithm" != "" else []
PROJECT_NAME = "rrt_star_algorithm"
PROJECT_SPACE_DIR = "/home/ubuntu/kmr-planning/devel"
PROJECT_VERSION = "0.0.0"
