# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/ubuntu/kmr-planning/src/planner_data_structures/include".split(';') if "/home/ubuntu/kmr-planning/src/planner_data_structures/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lplanner_data_structures".split(';') if "-lplanner_data_structures" != "" else []
PROJECT_NAME = "planner_data_structures"
PROJECT_SPACE_DIR = "/home/ubuntu/kmr-planning/devel"
PROJECT_VERSION = "0.0.0"
