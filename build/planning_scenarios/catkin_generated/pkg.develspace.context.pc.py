# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "".split(';') if "" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;planning_world_builder;birrt_star_algorithm;rrt_star_algorithm".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "planning_scenarios"
PROJECT_SPACE_DIR = "/home/ubuntu/kmr-planning/devel"
PROJECT_VERSION = "0.0.0"
