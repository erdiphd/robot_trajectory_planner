#include <ros/ros.h>

// MoveIt
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>


#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>


int main(int argc, char** argv)
{
  ros::init(argc, argv, "robot_model_and_robot_state_tutorial");
  ros::AsyncSpinner spinner(1);
  spinner.start();
  
  static const std::string PLANNING_GROUP = "robot_arm";

  // The :move_group_interface:`MoveGroupInterface` class can be easily
  // setup using just the name of the planning group you would like to control and plan for.
  moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);

//   robot_model_loader::RobotModelLoader robot_model_loader("/iiwa/robot_description");
//   const moveit::core::RobotModelPtr& kinematic_model = robot_model_loader.getModel();
//   ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());

//    moveit::core::RobotStatePtr kinematic_state(new moveit::core::RobotState(kinematic_model));
//   kinematic_state->setToDefaultValues();
//   const moveit::core::JointModelGroup* joint_model_group = kinematic_model->getJointModelGroup("robot_arm");

//   const std::vector<std::string>& joint_names = joint_model_group->getVariableNames();
  
  
//   std::vector<double> joint_values;
//   kinematic_state->copyJointGroupPositions(joint_model_group, joint_values);
//   for (std::size_t i = 0; i < joint_names.size(); ++i)
//   {
//     ROS_INFO("Joint %s: %f", joint_names[i].c_str(), joint_values[i]);
//   }


//   joint_values[0] = 5.57;
//   kinematic_state->setJointGroupPositions(joint_model_group, joint_values);

//   /* Check whether any joint is outside its joint limits */
//   ROS_INFO_STREAM("Current state is " << (kinematic_state->satisfiesBounds() ? "valid" : "not valid"));

//   /* Enforce the joint limits for this state and check again*/
//   kinematic_state->enforceBounds();
//   ROS_INFO_STREAM("Current state is " << (kinematic_state->satisfiesBounds() ? "valid" : "not valid"));


//     kinematic_state->setToRandomPositions(joint_model_group);
//   const Eigen::Isometry3d& end_effector_state = kinematic_state->getGlobalLinkTransform("iiwa_link_7");

//   /* Print end-effector pose. Remember that this is in the model frame */
//   ROS_INFO_STREAM("Translation: \n" << end_effector_state.translation() << "\n");
//   ROS_INFO_STREAM("Rotation: \n" << end_effector_state.rotation() << "\n");


}