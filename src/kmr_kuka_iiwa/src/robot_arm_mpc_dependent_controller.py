#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist
from gazebo_msgs.msg import ModelStates
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np 
import scipy.io
import tf2_ros
import moveit_commander
import moveit_msgs.msg
import sys
from std_msgs.msg import UInt32, Float64
import time
q = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/matlab_backstepping_container/q.mat')
q = np.array(q['q'])

def shutdown_function():
    print("Controller shut down")


class HanoiTowerPickup(object):
    """Hanoi Towers solver"""

    def __init__(self,namespace,robot_description):
        """ create configurations for MoveIt API
        :param namespace: (str) namespace of the robot model
        :param robot_description: (str) rosparam name of the robot model
        """
        super(HanoiTowerPickup,self).__init__()
        self.namespace = namespace
        self.robot_description = robot_description

        # Initialize the moveit commander and rospy
        moveit_commander.roscpp_initialize(sys.argv)

        #Initialize robot commander which provides information about robot's kinematic model and robot's current joint states
        self.robot = moveit_commander.RobotCommander(ns=self.namespace, robot_description=self.robot_description)

        iiwa_group_name = "robot_arm"
        self.move_group_joints = moveit_commander.MoveGroupCommander(iiwa_group_name,ns=self.namespace,robot_description=self.robot_description,wait_for_servers=20)

        grasp_group_name = "robot_gripper"
        self.move_group_gripper = moveit_commander.MoveGroupCommander(grasp_group_name,ns=self.namespace,robot_description=self.robot_description,wait_for_servers=20)

    
    def get_basic_information(self):
        """ Get basic robot information and print it out in terminal
        """
        # robot:
        print ("============ Printing robot state")
        print (self.robot.get_current_state())
        print("End effector link")
        print (self.move_group_joints.get_end_effector_link())
        print("Current Positon")
        current_pose = self.move_group_joints.get_current_pose().pose
        print(current_pose)
        print("robot pose")
        print(self.robot.get_current_state())
        print("Get current joint values:")
        print(self.move_group_joints.get_current_joint_values())

    def joint_command_test(self,theta3,theta4,theta5,theta6,theta7,theta8,theta9):
        joint_goal = self.move_group_joints.get_current_joint_values()

        joint_goal[0] = np.clip(theta3,-2.96706, 2.96706)
        joint_goal[1] = np.clip(theta4,-2.0944, 2.0944)
        joint_goal[2] = np.clip(theta5,-2.96706, 2.96706)
        joint_goal[3] = np.clip(theta6,-2.0944, 2.0944)
        joint_goal[4] = np.clip(theta7,-2.96706, 2.96706)
        joint_goal[5] = np.clip(theta8,-2.0944, 2.0944)
        joint_goal[6] = np.clip(theta9, -3.05433 , 3.05433)
        # time.sleep(1)
        print("joint goal:" ,joint_goal)
        try:
            self.move_group_joints.go(joint_goal, wait=True)
        except:
            print('stop')


def index_number(data):
    
    
    global index_variable
    if index_variable != data.data:
        print("index: ", data.data)
        theta3 = round(q[index_variable,2],4)
        theta4 = round(q[index_variable,3],4)
        theta5 = round(q[index_variable,4],4)
        theta6 = round(q[index_variable,5],4)
        theta7 = round(q[index_variable,6],4)
        theta8 = round(q[index_variable,7],4)
        theta9 = round(q[index_variable,8],4)

        theta3 = np.clip(theta3,-2.96706, 2.96706)
        theta4 = np.clip(theta4,-2.0944, 2.0944)
        theta5 = np.clip(theta5,-2.96706, 2.96706)
        theta6 = np.clip(theta6,-2.0944, 2.0944)
        theta7 = np.clip(theta7,-2.96706, 2.96706)
        theta8 = np.clip(theta8,-2.0944, 2.0944)
        theta9 = np.clip(theta9, -3.05433 , 3.05433)
        
        theta3_publisher.publish(theta3)
        theta4_publisher.publish(theta4)
        theta5_publisher.publish(theta5)
        theta6_publisher.publish(theta6)
        theta7_publisher.publish(theta7)
        theta8_publisher.publish(theta8)
        theta9_publisher.publish(theta9)
        print("theta3: ", theta3," theta4: ",theta4," theta5: ",theta5," theta6: ",theta6," theta7: ",theta7," theta8: ",theta8," theta9: ",theta9)

    
    index_variable = data.data

    


while_loop_counter = 0
if __name__ == "__main__":

    # ROS Configurations

    #Controller frequency = 1/delta
    delta_t = 1
    index_variable = 0 
    rospy.init_node("robot_arm_mpc_dependent_controller")

    rospy.Subscriber("desired_trajectory_idx", UInt32, index_number)

    theta3_publisher = rospy.Publisher('/iiwa/arm_j1_position_controller/command', Float64, queue_size=10)
    theta4_publisher = rospy.Publisher('/iiwa/arm_j2_position_controller/command', Float64, queue_size=10)
    theta5_publisher = rospy.Publisher('/iiwa/arm_j3_position_controller/command', Float64, queue_size=10)
    theta6_publisher = rospy.Publisher('/iiwa/arm_j4_position_controller/command', Float64, queue_size=10)
    theta7_publisher = rospy.Publisher('/iiwa/arm_j5_position_controller/command', Float64, queue_size=10)
    theta8_publisher = rospy.Publisher('/iiwa/arm_j6_position_controller/command', Float64, queue_size=10)
    theta9_publisher = rospy.Publisher('/iiwa/arm_j7_position_controller/command', Float64, queue_size=10)

    rate = rospy.Rate(1/delta_t) # 10hz
    rospy.on_shutdown(shutdown_function)

    # theta4 = 0.85
    # for index_variable in range(len(q))[240:262]:
    #     print("index_variable: ", index_variable)
    #     theta3 = 0
    #     theta4 += 0.01
    #     theta5 = 0
    #     theta6 = 0
    #     theta7 = 0
    #     theta8 = 0
    #     theta9 = 0
    #     theta3_publisher.publish(theta3)
    #     theta4_publisher.publish(theta4)
    #     theta5_publisher.publish(theta5)
    #     theta6_publisher.publish(theta6)
    #     theta7_publisher.publish(theta7)
    #     theta8_publisher.publish(theta8)
    #     theta9_publisher.publish(theta9)
    #     rate.sleep()

        
    rate.sleep()

    
    
    rospy.spin()