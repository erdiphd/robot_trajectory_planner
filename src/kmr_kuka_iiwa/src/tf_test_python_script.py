#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist
from gazebo_msgs.msg import ModelStates
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np 
import scipy.io
import tf2_ros
import moveit_commander
import moveit_msgs.msg
import sys


rospy.init_node("tf_test")
tfBuffer = tf2_ros.Buffer()
listener = tf2_ros.TransformListener(tfBuffer)
delta_t = 0.05
rate = rospy.Rate(1/delta_t) # 10hz
while not rospy.is_shutdown():
    get_odom_data = False
    try:
        trans = tfBuffer.lookup_transform('odom', 'base_link', rospy.Time(0))
        get_odom_data = True
        print(trans)


    except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
        print("In Except situation")
        continue
    rate.sleep()
        