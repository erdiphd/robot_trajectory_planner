#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist
from gazebo_msgs.msg import ModelStates
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np 
import scipy.io
import tf2_ros


# iteration_counter = 0
trajectory_x = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/trajectory_x.mat')
trajectory_x = np.array(trajectory_x['trajectory_x'])

trajectory_y = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/trajectory_y.mat')
trajectory_y = np.array(trajectory_y['trajectory_y'])

trajectory_phi = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/phi_container.mat')
trajectory_phi = np.array(trajectory_phi['phi_container'])


velocity_container = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/velocity.mat')
velocity_container = np.array(velocity_container['velocity_container'])

angular_container = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/angular.mat')
angular_container = np.array(angular_container['angular_container'])

tmp_time = 0

real_trajectory_x = []
real_trajectory_y = []

desired_trajectory_x = []
desired_trajectory_y = []

desired_pos_x = 0
desired_pos_y = 0
theta_desired = 0


def limit_input(v,w):
    v_max, w_max = 0.2, 0.5

    if v >= v_max:
        v = v_max
    if v<= -v_max:
        v = -v_max
    if w>= w_max:
        w = w_max
    if w<= -w_max:
        w = -w_max

    return v,w
    
def shutdown_function():
    print("Controller shut down")
    twist = Twist()
    twist.linear.x = 0.0; twist.linear.y = 0.0; twist.linear.z = 0.0
    twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = 0.0
    velocity_publisher.publish(twist)

while_loop_counter = 0
if __name__ == "__main__":

    # ROS Configurations

    #Controller frequency = 1/delta
    delta_t = 1
    rospy.init_node("kmr_kuka_robot_mobile_part_controller")
    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)

    rate = rospy.Rate(1/delta_t) # 10hz
    velocity_publisher = rospy.Publisher('cmd_vel', Twist, queue_size=10)
    rospy.on_shutdown(shutdown_function)

    integral_error_v = 0
    integral_error_w = 0
    prev_v = 0
    prev_w = 0

    while not rospy.is_shutdown():
        get_odom_data = False
        try:
            trans = tfBuffer.lookup_transform('odom', 'base_link', rospy.Time(5))
            get_odom_data = True
            print(trans.transform.translation)
            while_loop_counter += 1


        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print("In Except situation")
            continue
        
        if get_odom_data:
            eul_rot = euler_from_quaternion([trans.transform.rotation.x,trans.transform.rotation.y,trans.transform.rotation.z,trans.transform.rotation.w])

            theta = eul_rot[2] - math.atan(trajectory_y[0,while_loop_counter]/trajectory_x[0,while_loop_counter])
            ex = (trajectory_x[0,while_loop_counter] - trans.transform.translation.x) * math.cos(theta) + (trajectory_y[0,while_loop_counter] - trans.transform.translation.y) * math.sin(theta)
            ey = (trajectory_y[0,while_loop_counter] - trans.transform.translation.y) * math.cos(theta) - (trajectory_x[0,while_loop_counter] - trans.transform.translation.y) * math.sin(theta)
            etheta = (np.arctan2(trajectory_x[0,while_loop_counter],trajectory_y[0,while_loop_counter]) - eul_rot[2])
            

            # integral_error_v = integral_error_v + v_e * delta_t
            # integral_error_w = integral_error_w + w_e * delta_t

            v_in = 2 * ex 
            w_in = 2 * ey +  2* etheta

            v_in, w_in = limit_input(v_in,w_in)

            twist = Twist()
            twist.linear.x = v_in
            twist.linear.y = 0.0
            twist.linear.z = 0.0
            twist.angular.x = 0.0
            twist.angular.y = 0.0
            twist.angular.z = w_in
            
            velocity_publisher.publish(twist)

            # print(rospy.get_time())
            print(v_in)
            # prev_v = v_e
            # prev_w = w_e
        rate.sleep()

    
    
    rospy.spin()