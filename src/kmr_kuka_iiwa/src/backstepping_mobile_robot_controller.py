#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist
from gazebo_msgs.msg import ModelStates
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np 
import scipy.io
import tf2_ros


# iteration_counter = 0
x_trajectory = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/matlab_backstepping_container/x_trajectory.mat')
x_trajectory = np.array(x_trajectory['x_trajectory'])

y_trajectory = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/matlab_backstepping_container/y_trajectory.mat')
y_trajectory = np.array(y_trajectory['y_trajectory'])

theta_trajectory = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/matlab_backstepping_container/theta_trajectory.mat')
theta_trajectory = np.array(theta_trajectory['theta_trajectory'])


trans_vel = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/matlab_backstepping_container/trans_vel.mat')
trans_vel = np.array(trans_vel['trans_vel'])


angular_vel = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/matlab_backstepping_container/angular_vel.mat')
angular_vel = np.array(angular_vel['angular_vel'])

desired_pos_x = 0
desired_pos_y = 0
theta_desired = 0



def BackSteppingController(desired_vel,desired_pos,current):
    """
    desired_vel is 2x1 array with (v and w)
    desired_pos is 3x1 array with (x_r,y_r,theta_r)
    current is 3x1 array with (x,y,theta)
    """

    theta = desired_pos[2]
    k1 = 20
    k2 = 3
    k3 = 3
    inverse_rotation_matrix_z = np.array([[np.cos(theta),np.sin(theta),0],[-np.sin(theta),np.cos(theta),0],[0,0,1]])

    error_vector = inverse_rotation_matrix_z @ np.array([[desired_pos[0] - current[0]],[desired_pos[1] - current[1]],[desired_pos[2] - current[2]]])

    vc = desired_vel[0] * math.cos(error_vector[2]) + k1 * error_vector[0]
    wc = desired_vel[1] + k2 * desired_vel[0] * error_vector[1]  + k3 * math.sin(error_vector[2])

    # print("desired positon: ", desired)
    # print("current position: ", current)
    # print("error vector in robot frame : ", error_vector)
    # print("error vector in global frame: " ,np.array(desired) - np.array(current))


    return vc,wc

def limit_input(v_x,v_y,w):
    v_max, w_max = 0.5, 0.3

    if v_x >= v_max:
        v_x = v_max
    if v_x <= -v_max:
        v_x = -v_max
    if w>= w_max:
        w = w_max
    if w<= -w_max:
        w = -w_max
    if v_y >= v_max:
        v_y = v_max
    if v_y <= -v_max:
        v_y = -v_max

    return v_x,v_y,w
    
def shutdown_function():
    print("Controller shut down")
    twist = Twist()
    twist.linear.x = 0.0; twist.linear.y = 0.0; twist.linear.z = 0.0
    twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = 0.0
    velocity_publisher.publish(twist)

while_loop_counter = 0
if __name__ == "__main__":

    # ROS Configurations

    #Controller frequency = 1/delta
    delta_t = 0.05
    rospy.init_node("kmr_kuka_robot_mobile_part_controller")
    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)

    rate = rospy.Rate(1/delta_t) # 10hz
    velocity_publisher = rospy.Publisher('cmd_vel', Twist, queue_size=10)
    rospy.on_shutdown(shutdown_function)

    integral_error_v = 0
    integral_error_w = 0
    prev_v = 0
    prev_w = 0

    while not rospy.is_shutdown():
        get_odom_data = False
        try:
            trans = tfBuffer.lookup_transform('odom', 'base_link', rospy.Time(5))
            get_odom_data = True


        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print("In Except situation")
            continue
        
        if get_odom_data:
            eul_rot = euler_from_quaternion([trans.transform.rotation.x,trans.transform.rotation.y,trans.transform.rotation.z,trans.transform.rotation.w])
            print("Robot Position:","X: ",trans.transform.translation.x, "Y: ", trans.transform.translation.y, "Theta: ", eul_rot[2])
            
            
            
            current_position = [trans.transform.translation.x, trans.transform.translation.y,eul_rot[2]]
            desired_theta = np.arctan2(x_trajectory[while_loop_counter,0],y_trajectory[while_loop_counter,0])
            desired_position = [x_trajectory[while_loop_counter,0],y_trajectory[while_loop_counter,0],theta_trajectory[while_loop_counter,0]]
            desired_vel = [trans_vel[while_loop_counter,0],angular_vel[while_loop_counter,0]]
            v_c, w_c = BackSteppingController(desired_vel, desired_position,current_position)
    
            # v_c,v_c1,w_c = limit_input(v_c,v_c,w_c)

            twist = Twist()
            twist.linear.x = v_c
            twist.linear.y = 0
            twist.linear.z = 0.0
            twist.angular.x = 0.0
            twist.angular.y = 0.0
            twist.angular.z = w_c
            
            velocity_publisher.publish(twist)

            print("while_loop_counter: ",while_loop_counter)
            while_loop_counter = while_loop_counter + 1

        rate.sleep()

    
    
    rospy.spin()