import matplotlib.pyplot as plt
import numpy as np 
import scipy.io
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure(figsize=(4,4))

ax = fig.add_subplot(111, projection='3d')
trajectory_x = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/mobile_robot_trajectory_x.npy')
trajectory_y = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/mobile_robot_trajectory_y.npy')
trajectory_z = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/mobile_robot_trajectory_z.npy')



ax.plot(trajectory_x,trajectory_y,trajectory_z, label='parametric curve')

# ax.scatter(trajectory_x,trajectory_y,trajectory_z)
plt.show()


# trajectory_x = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/mobile_robot_trajectory_x.npy')
# trajectory_y = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/mobile_robot_trajectory_y.npy')

# x_trajectory = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/matlab_backstepping_container/x_trajectory.mat')
# desired_x_trajectory = np.array(x_trajectory['x_trajectory'])

# y_trajectory = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/matlab_backstepping_container/y_trajectory.mat')
# desired_y_trajectory = np.array(y_trajectory['y_trajectory'])

# plt.plot(trajectory_x,trajectory_y,'r.' )
# plt.plot(desired_x_trajectory,desired_y_trajectory,'b.' )


# plt.plot(desired_trajectory_x,desired_trajectory_y )
plt.ylabel('some numbers')
plt.show()
