#!/usr/bin/env python3


import numpy as np
import rospy
from geometry_msgs.msg import Twist
velocity_container = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/velocity_container.npy')
omega_container = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/omega_container.npy')

print(omega_container.shape)


def shutdown_function():
    print("Controller shut down")
    twist = Twist()
    twist.linear.x = 0.0; twist.linear.y = 0.0; twist.linear.z = 0.0
    twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = 0.0
    velocity_publisher.publish(twist)



if __name__ == "__main__":

    # ROS Configurations

    #Controller frequency = 1/delta
    rospy.init_node("kmr_kuka_robot_mobile_part_controller")
    rate = rospy.Rate(1/0.15) # 10hz
    velocity_publisher = rospy.Publisher('cmd_vel', Twist, queue_size=10)
    rospy.on_shutdown(shutdown_function)

    rospy.loginfo("Commander is initialized...")


    while_loop_counter = 0
    
    while not rospy.is_shutdown():
       
        twist = Twist()
        twist.linear.x = velocity_container[while_loop_counter]
        twist.linear.y = 0
        twist.linear.z = 0.0
        twist.angular.x = 0.0
        twist.angular.y = 0.0
        twist.angular.z = omega_container[while_loop_counter]

        print("while_loop_counter: ",while_loop_counter)

        
        velocity_publisher.publish(twist)

        while_loop_counter+=1
        rate.sleep()

    
    
    rospy.spin()