#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist
from gazebo_msgs.msg import ModelStates
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np 
integral_error_ex = 0
integral_error_ey = 0
integral_error_etheta = 0
prev_ex = 0 
prev_ey = 0 
prev_etheta = 0
integral_error_ex_array = []

def shutdown_function():
    print("Controller shut down")
    twist = Twist()
    twist.linear.x = 0.0; twist.linear.y = 0.0; twist.linear.z = 0.0
    twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = 0.0
    velocity_publisher.publish(twist)

def model_position(data):

    object_list_in_gazebo = data.name
    all_objects_startswith = [i for i in data.name if i.startswith("kmr")] # All objects in Gazebo starts with specified string 
    kmr_index_number_in_object_list = object_list_in_gazebo.index('kmr')
    
    mobile_robot_position = data.pose[kmr_index_number_in_object_list].position
    mobile_robot_orientation = data.pose[kmr_index_number_in_object_list].orientation

    (robot_roll, robot_pitch, robot_yaw) = euler_from_quaternion([mobile_robot_orientation.x,mobile_robot_orientation.y,mobile_robot_orientation.z,mobile_robot_orientation.w])

    print("Robot yaw " ,robot_yaw)
    twist_target = Twist()
    twist_target.linear.x = 2
    twist_target.linear.y = 2
    twist_target.linear.z = 0
    twist_target.angular.x = 0
    twist_target.angular.y = 0
    twist_target.angular.z = 0
    

    twist = Twist()
    twist.linear.x = 0.0; twist.linear.y = 0.0; twist.linear.z = 0.0
    twist.angular.x = 0.0; twist.angular.y = 0.0;

    global integral_error_ex
    global integral_error_ey
    global integral_error_etheta
    global prev_ex
    global prev_ey
    global prev_etheta
    global integral_error_ex_array

    theta  = robot_yaw - twist_target.angular.z 
    ex = (twist_target.linear.x - mobile_robot_position.x) * math.cos(theta) + (twist_target.linear.y - mobile_robot_position.y) * math.sin(theta)
    ey = (twist_target.linear.y - mobile_robot_position.y) * math.cos(theta) - (twist_target.linear.x - mobile_robot_position.x) * math.sin(theta)
    etheta = twist_target.angular.z - robot_yaw


    integral_error_ex = integral_error_ex + ex * 1/10
    integral_error_ey = integral_error_ey + ey * 1/10
    integral_error_etheta = integral_error_etheta + etheta * 1/10 


    integral_error_ex_array.append(ex)

    twist.linear.x =  ex + 0.01* integral_error_ex + (ex - prev_ex )/0.1
    twist.angular.z  =  ey + 0.01* integral_error_ey + (ey - prev_ey )/0.1  +  etheta + 0.01* integral_error_etheta + (etheta - prev_etheta )/0.1






    print("Target Position Value: ", twist.linear.x)
    print("Target Angle Value: ", twist_target.angular.z )
    print("twist.angular.z: ", twist.angular.z )

    print("Position differnce:" , twist_target.linear.x - mobile_robot_position.x)


    print("Integral error:", integral_error_ex)

    velocity_publisher.publish(twist)

    prev_ex = ex
    prev_ey = ey
    prev_etheta = etheta

    np.save('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/error_x.npy', np.array(integral_error_ex_array))


if __name__ == "__main__":

    # ROS Configurations
    rospy.init_node("kmr_kuka_robot_mobile_part_controller")
    velocity_publisher = rospy.Publisher('cmd_vel', Twist, queue_size=10)
    rospy.Subscriber("/gazebo/model_states", ModelStates, model_position)
    rate = rospy.Rate(10) # 10hz
    rospy.on_shutdown(shutdown_function)

    twist = Twist()
    # twist.linear.x = 0.0; twist.linear.y = 0.0; twist.linear.z = 0.0
    # twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = 0.0
    while not rospy.is_shutdown():
        # velocity_publisher.publish(twist)
        rate.sleep()
    

    rospy.spin()