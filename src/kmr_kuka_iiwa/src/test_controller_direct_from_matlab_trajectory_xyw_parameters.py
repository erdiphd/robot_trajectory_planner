#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist
from gazebo_msgs.msg import ModelStates
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np 
import scipy.io
import tf2_ros


# iteration_counter = 0
trajectory_x = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/rx_last.mat')
trajectory_x = np.array(trajectory_x['rx_last'])

trajectory_y = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/ry_last.mat')
trajectory_y = np.array(trajectory_y['ry_last'])

desired_pos_x = 0
desired_pos_y = 0
theta_desired = 0


integral_error = np.zeros([3,1])
previous_error = np.zeros([3,1])

def PIDcontroller(desired,current):

    global integral_error
    global previous_error

    Kp = 1
    Ki = 0
    Kd = 0

    theta = current[2]

    inverse_rotation_matrix_z = np.array([[np.cos(theta),np.sin(theta),0],[-np.sin(theta),np.cos(theta),0],[0,0,1]])

    error_vector = inverse_rotation_matrix_z @ np.array([[desired[0] - current[0]],[desired[1] - current[1]],[desired[2] - current[2]]])

    print("desired positon: ", desired)
    print("current position: ", current)
    print("error vector in robot frame : ", error_vector)
    print("error vector in global frame: " ,np.array(desired) - np.array(current))



    integral_error = integral_error + error_vector * delta_t
    derivative_error = (error_vector - previous_error) / delta_t

    v_in_x = Kp * error_vector[0] + Ki * integral_error[0] + Kd * derivative_error[0]
    v_in_y = Kp * error_vector[1] + Ki * integral_error[1] + Kd * derivative_error[1]
    w_in = Kp* error_vector[2] + Ki * integral_error[2]  + Kd * derivative_error[2]







    print("Robot input parameters before limit: ", "v_in_x: ", v_in_x, "v_in_y: ", v_in_y, "w_in: ",w_in)

    v_in_x,v_in_y, w_in = limit_input(v_in_x,v_in_y,w_in)

    print("Robot input parameters after limit: ", "v_in_x: ", v_in_x, "v_in_y: ", v_in_y, "w_in: ",w_in)

    previous_error  = error_vector
    return v_in_x , v_in_y, w_in, error_vector

def limit_input(v_x,v_y,w):
    v_max, w_max = 0.5, 0.3

    if v_x >= v_max:
        v_x = v_max
    if v_x <= -v_max:
        v_x = -v_max
    if w>= w_max:
        w = w_max
    if w<= -w_max:
        w = -w_max
    if v_y >= v_max:
        v_y = v_max
    if v_y <= -v_max:
        v_y = -v_max

    return v_x,v_y,w
    
def shutdown_function():
    print("Controller shut down")
    twist = Twist()
    twist.linear.x = 0.0; twist.linear.y = 0.0; twist.linear.z = 0.0
    twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = 0.0
    velocity_publisher.publish(twist)

while_loop_counter = 0
if __name__ == "__main__":

    # ROS Configurations

    #Controller frequency = 1/delta
    delta_t = 0.01
    rospy.init_node("kmr_kuka_robot_mobile_part_controller")
    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)

    rate = rospy.Rate(1/delta_t) # 10hz
    velocity_publisher = rospy.Publisher('cmd_vel', Twist, queue_size=10)
    rospy.on_shutdown(shutdown_function)

    integral_error_v = 0
    integral_error_w = 0
    prev_v = 0
    prev_w = 0

    while not rospy.is_shutdown():
        get_odom_data = False
        try:
            trans = tfBuffer.lookup_transform('odom', 'base_link', rospy.Time(5))
            get_odom_data = True


        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print("In Except situation")
            continue
        
        if get_odom_data:
            eul_rot = euler_from_quaternion([trans.transform.rotation.x,trans.transform.rotation.y,trans.transform.rotation.z,trans.transform.rotation.w])
            print("Robot Position:","X: ",trans.transform.translation.x, "Y: ", trans.transform.translation.y, "Theta: ", eul_rot[2])
            
            
            
            current_position = [trans.transform.translation.x, trans.transform.translation.y,eul_rot[2]]
            desired_theta = np.arctan2(trajectory_x[0,while_loop_counter],trajectory_y[0,while_loop_counter])
            desired_position = [trajectory_x[0,while_loop_counter],trajectory_y[0,while_loop_counter],desired_theta]

            v_in_x , v_in_y, w_in, error_vector = PIDcontroller(desired_position,current_position)
    
            if np.abs(error_vector[0]) < 0.1 and np.abs(error_vector[1]) < 0.3 and np.abs(error_vector[2]) < 0.1:
                while_loop_counter += 1


            

            twist = Twist()
            twist.linear.x = v_in_x + v_in_y
            twist.linear.y = 0
            twist.linear.z = 0.0
            twist.angular.x = 0.0
            twist.angular.y = 0.0
            twist.angular.z = w_in + v_in_y
            
            velocity_publisher.publish(twist)
        rate.sleep()

    
    
    rospy.spin()