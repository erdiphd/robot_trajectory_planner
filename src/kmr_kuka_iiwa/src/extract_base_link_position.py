#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist
from gazebo_msgs.msg import LinkStates
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np 


trajectory_x = []
trajectory_y = []
trajectory_z = []

def shutdown_function():
    print("Controller shut down")
    twist = Twist()
    twist.linear.x = 0.0; twist.linear.y = 0.0; twist.linear.z = 0.0
    twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = 0.0
    velocity_publisher.publish(twist)

def model_position(data):

    object_list_in_gazebo = data.name
    all_objects_startswith = [i for i in data.name if i.startswith("kmr::iiwa_link_7")] # All objects in Gazebo starts with specified string 
    kmr_index_number_in_object_list = object_list_in_gazebo.index('kmr::iiwa_link_7')
    
    mobile_robot_position = data.pose[kmr_index_number_in_object_list].position
    mobile_robot_orientation = data.pose[kmr_index_number_in_object_list].orientation

    global trajectory_x
    global trajectory_y
    global trajectory_z

    trajectory_x.append(mobile_robot_position.x)
    trajectory_y.append(mobile_robot_position.y)
    trajectory_z.append(mobile_robot_position.z)

    np.save('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/mobile_robot_trajectory_x.npy',np.array(trajectory_x))
    np.save('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/mobile_robot_trajectory_y.npy',np.array(trajectory_y))
    np.save('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/mobile_robot_trajectory_z.npy',np.array(trajectory_z))

if __name__ == "__main__":

    # ROS Configurations
    rospy.init_node("kmr_kuka_robot_trajectory_extractor")
    velocity_publisher = rospy.Publisher('cmd_vel', Twist, queue_size=10)
    rospy.Subscriber("/gazebo/link_states", LinkStates, model_position)
    rate = rospy.Rate(10)
    rospy.on_shutdown(shutdown_function)

    twist = Twist()
    # twist.linear.x = 0.0; twist.linear.y = 0.0; twist.linear.z = 0.0
    # twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = 0.0
    while not rospy.is_shutdown():
        # velocity_publisher.publish(twist)
        rate.sleep()
    

    rospy.spin()