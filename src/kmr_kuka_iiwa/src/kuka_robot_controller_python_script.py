#! /usr/bin/env python
import sys
import rospy
import rospkg
import geometry_msgs.msg
# sys.path.append("/home/erdi/Desktop/Storage/Temporary/GIT/Github/moveit/moveit_commander/src")
import moveit_commander
import moveit_msgs.msg
import copy
# rospy.sleep(10)
import tf
import numpy as np
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion
from std_msgs.msg import Header
from math import pi as pi_number
origin, xaxis, yaxis, zaxis = (0, 0, 0), (1, 0, 0), (0, 1, 0), (0, 0, 1)


class HanoiTowerPickup(object):
    """Hanoi Towers solver"""

    def __init__(self,namespace,robot_description):
        """ create configurations for MoveIt API
        :param namespace: (str) namespace of the robot model
        :param robot_description: (str) rosparam name of the robot model
        """
        super(HanoiTowerPickup,self).__init__()
        self.namespace = namespace
        self.robot_description = robot_description

        # Initialize the moveit commander and rospy
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('move_group_python_interface_for_hanoi_tower', anonymous=True)

        #Initialize robot commander which provides information about robot's kinematic model and robot's current joint states
        self.robot = moveit_commander.RobotCommander(ns=self.namespace, robot_description=self.robot_description)

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        self.scene = moveit_commander.PlanningSceneInterface(ns=self.namespace,synchronous=True)

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).  In this tutorial the group is the primary
        ## arm joints in the Panda robot, so we set the group's name to "panda_arm".
        ## If you are using a different robot, change this value to the name of your robot
        ## arm planning group.
        ## This interface can be used to plan and execute motions:

        iiwa_group_name = "robot_arm"
        self.move_group_joints = moveit_commander.MoveGroupCommander(iiwa_group_name,ns=self.namespace,robot_description=self.robot_description,wait_for_servers=20)

        grasp_group_name = "robot_gripper"
        self.move_group_gripper = moveit_commander.MoveGroupCommander(grasp_group_name,ns=self.namespace,robot_description=self.robot_description,wait_for_servers=20)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        self.display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=20)

        #Initialize ROS TF
        self.listener = tf.TransformListener()

        #wait for the following reference frame to be ready
        self.listener.waitForTransform("/odom", "/iiwa_link_7", rospy.Time(), rospy.Duration(4.0))

        #Token (Disk) height (measured from the mesh file)
        self.token_height = 0.017

        #Set precision for the numpy array
        np.set_printoptions(precision=2)

    def get_basic_information(self):
        """ Get basic robot information and print it out in terminal
        """
        # robot:
        print ("============ Printing robot state")
        print (self.robot.get_current_state())
        print("End effector link")
        print (self.move_group_joints.get_end_effector_link())
        print("Current Positon")
        current_pose = self.move_group_joints.get_current_pose().pose
        print(current_pose)
        print("robot pose")
        print(self.robot.get_current_state())
        print("Get current joint values:")
        print(self.move_group_joints.get_current_joint_values())

    
    def joint_command_test(self):
        joint_goal = self.move_group_joints.get_current_joint_values()
        join_goal = [0.0001219721166746026, -1.0355420801728896, 0.00022971388083903915, -2.1852135411020264, 2.5409538794060705e-05, 1.1792796490087125, 0.0]
        print("joint_goal:", joint_goal)
        self.move_group_joints.go(joint_goal, wait=True)


    def wait_for_state_update(self, box_is_known=False, box_is_attached=False, timeout=4):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        box_name = "mybox"
        scene = self.scene

        ## BEGIN_SUB_TUTORIAL wait_for_scene_update
        ##
        ## Ensuring Collision Updates Are Receieved
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        ## If the Python node dies before publishing a collision object update message, the message
        ## could get lost and the box will not appear. To ensure that the updates are
        ## made, we wait until we see the changes reflected in the
        ## ``get_attached_objects()`` and ``get_known_object_names()`` lists.
        ## For the purpose of this tutorial, we call this function after adding,
        ## removing, attaching or detaching an object in the planning scene. We then wait
        ## until the updates have been made or ``timeout`` seconds have passed
        start = rospy.get_time()
        seconds = rospy.get_time()
        while (seconds - start < timeout) and not rospy.is_shutdown():
            # Test if the box is in attached objects
            attached_objects = scene.get_attached_objects([box_name])
            is_attached = len(attached_objects.keys()) > 0

            # Test if the box is in the scene.
            # Note that attaching the box will remove it from known_objects
            is_known = box_name in scene.get_known_object_names()

            # Test if we are in the expected state
            if (box_is_attached == is_attached) and (box_is_known == is_known):
                return True

            # Sleep so that we give other threads time on the processor
            rospy.sleep(0.1)
            seconds = rospy.get_time()

        # If we exited the while loop without returning then we timed out
        return False
        ## END_SUB_TUTORIAL


    def add_and_attach_mesh(self, timeout=4):
        """Add and attach mesh file in planing scene in RViz .

        :param timeout: Time to wait for update
        :type state: string, optional
        :return: Return an update function
        :rtype: Bool
        """
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        scene = self.scene

        # get an instance of RosPack with the default search paths
        rospack = rospkg.RosPack()
        # get the file path for rospy_tutorials
        rospkg_name = rospack.get_path('hanoi_tower_kuka_model')

        ## BEGIN_SUB_TUTORIAL add_box
        ##
        ## Adding Objects to the Planning Scene
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        ## First, we will create a box in the planning scene at the location of the left finger:
        loc,rot = self.get_pos_and_rot_between_frames('world',"hanoi_board_middle_below_reference_frame")
        p = PoseStamped()
        p.header.frame_id = self.robot.get_planning_frame()
        p.pose.position.x = loc[0]
        p.pose.position.y = loc[1]
        p.pose.position.z = loc[2]

        p.pose.orientation.x = rot[0]
        p.pose.orientation.y = rot[1]
        p.pose.orientation.z = rot[2]
        p.pose.orientation.w = rot[3]

        filename = rospkg_name + "/models/hanoi_tower_models/hanoi_board/meshes/hanoi-board.stl"
        box_name = "hanoi_board"
        scene.add_mesh(box_name, p, filename)
        print("Adding box to the scene.")




        token_file_name = "/models/hanoi_tower_models/hanoi_tower_d300/meshes/token-300.stl"
        gazebo_model_token_name = "hanoi_tower_d300_frame"
        for i in ["300","400","500","600","700"]:
            gazebo_model_token_name_replaced = gazebo_model_token_name.replace("300",i)
            loc,rot = self.get_pos_and_rot_between_frames('world',gazebo_model_token_name_replaced)
            p = PoseStamped()
            p.header.frame_id = self.robot.get_planning_frame()
            p.pose.position.x = loc[0]
            p.pose.position.y = loc[1]
            p.pose.position.z = loc[2]

            p.pose.orientation.x = rot[0]
            p.pose.orientation.y = rot[1]
            p.pose.orientation.z = rot[2]
            p.pose.orientation.w = rot[3]
            token_file_name_replaced = token_file_name.replace("300",i)
            filename = rospkg_name + token_file_name_replaced
            box_name = "token_d300".replace("300",i)
            scene.add_mesh(box_name, p, filename)
            print("Adding " + box_name + " to the scene.")

            grasping_group = 'grasp_plan_group'
            eef_link = self.move_group_joints.get_end_effector_link()
            touch_links = self.robot.get_link_names(group=grasping_group)
            self.scene.attach_mesh(self.robot.get_planning_frame(), box_name, touch_links=touch_links)

        ## END_SUB_TUTORIAL
        # Copy local variables back to class variables. In practice, you should use the class
        # variables directly unless you have a good reason not to.
        box_name=box_name
        return self.wait_for_state_update(box_is_known=True, timeout=timeout)

    def go_to_pose_goal(self,pos,rot,cartesian_path=False):
        """ Set the given position and rotation variables into the Pose() message
            and move the robot that orientation and location.

        :param pos: (list) position vector [x,y,z] in Cartesian coordinate system
                    For example; pos = [0.04 0.5  0.68]

        :param rot: (list) rotation vector [x,y,z,w] in quaternion system
                    For example; rot = [-1.13e-03  1.00  5.24e-04 -7.75e-04]

        :param cartesian_path: (boolean) Moves robot in cartesian path

        .. math::

            a^2 + b^2 = c^2

            a^2 + b^2 = c^2

        .. image:: media/rod.png


        """

        move_group = self.move_group_joints

        #Set given rotation and position vector to the Pose() message type so that MoveIt is able to use it.
        pose_target = geometry_msgs.msg.Pose()
        pose_target.orientation.x = rot[0]
        pose_target.orientation.y = rot[1]
        pose_target.orientation.z = rot[2]
        pose_target.orientation.w = rot[3]

        pose_target.position.x = pos[0]
        pose_target.position.y = pos[1]
        pose_target.position.z = pos[2]

        if not cartesian_path:
            move_group.set_pose_target(pose_target)
            plan = move_group.go(wait=True)
        else:
            (plan, fraction) = self.move_group_joints.compute_cartesian_path(
                                           [pose_target],   # waypoints to follow
                                           0.01,        # eef_step
                                           1.0)         # jump_threshold
            self.move_group_joints.execute(plan, wait=True)

        move_group.stop()
        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets()
        move_group.clear_pose_targets()
        current_pose = move_group.get_current_pose().pose
        print("Current Pose: ",current_pose )

    def move_weiter_test(self):
        print('Opening Gripper')
        self.move_group_gripper.set_named_target("gripper_open")
        self.move_group_gripper.go()

        move_group = self.move_group_joints
        pose_target = geometry_msgs.msg.Pose()
        pose_target.orientation.x = -0.00688338920234
        pose_target.orientation.y = 0.999950965998
        pose_target.orientation.z = 0.00107987017122
        pose_target.orientation.w = 0.00703693354945
        # They are for iiwa_link_ee position
        pose_target.position.x = 0.040
        pose_target.position.y = 0.60
        pose_target.position.z =  0.75
        move_group.set_pose_target(pose_target)
        plan = move_group.go(wait=True)
        move_group.stop()

        while pose_target.position.z >= 0.65:
            pose_target.position.z = pose_target.position.z - 0.01
            move_group.set_pose_target(pose_target)
            plan = move_group.go(wait=True)
            print("Z:",pose_target.position.z)

        # self.move_group_gripper.set_named_target("gripper_closed")
        # self.move_group_gripper.go()
        # print("gripper closed")
        # move_group.set_pose_target(pose_target)
        # plan = move_group.go(wait=True)

        # move_group.stop()

        # pose_target = geometry_msgs.msg.Pose()
        # pose_target.orientation.x = -0.00688338920234
        # pose_target.orientation.y = 0.999950965998
        # pose_target.orientation.z = 0.00107987017122
        # pose_target.orientation.w = 0.00703693354945

        # pose_target.position.x = 0.040
        # pose_target.position.y = 0.60
        # pose_target.position.z =  0.75
        # move_group.set_pose_target(pose_target)
        # plan = move_group.go(wait=True)
        # move_group.stop()

        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets()
        move_group.clear_pose_targets()
        current_pose = move_group.get_current_pose().pose


    def go_to_frame(self,name_of_frame="hanoi_tower_d300_frame",tolerance = [0,0,0],cartesian_path=False):
        """Move the robot to the given frame name with a specified tolerance vector in x,y,z

        :param name_of_frame: (str) name of the reference frame available in ROS TF
                    For example; name_of_frame="hanoi_tower_d300_frame" is the token reference frame

        :param tolerance: (list) position vector [x,y,z] in Cartesian coordinate system. It determines how close robot should approach to the specified frame.
                    For example; tolerance = [0,0,0.1]

        .. math::

            T^{world}_{end\_effector\_frame} = T^{world}_{token\_300} \cdot R_{y}(\pi)

            T^{world}_{iiwa\_link\_ee} =T^{world}_{end\_effector\_frame} \cdot T^{end\_effector\_frame}_{iiwa\_link\_ee}

        .. image:: media/reference_frames.png


        """
        #Preprocess of the desired location with respec to the iiwa_link_ee frame
        loc,rot = self.get_pos_and_rot_between_frames('world',name_of_frame)
        TF_token_to_world = self.get_transformation_matrix(loc,rot)
        end_effector_target = np.matmul(TF_token_to_world,self.Ry(pi_number))
        loc,rot = self.get_pos_and_rot_between_frames('end_effector_frame','iiwa_link_ee')
        TF_iiwa_link_ee_to_end_effector_frame = self.get_transformation_matrix(loc,rot)
        TF_iiwa_link_ee_to_world = np.matmul(end_effector_target,TF_iiwa_link_ee_to_end_effector_frame)
        rot=tf.transformations.quaternion_from_matrix(TF_iiwa_link_ee_to_world)
        pos = TF_iiwa_link_ee_to_world[0:3,-1]
        print("Target orientation of iiwa_link_ee",rot)
        pos[2] = pos[2] + self.token_height/ 2
        pos[0] = pos[0] + tolerance[0]
        pos[1] = pos[1] + tolerance[1]
        pos[2] = pos[2] + tolerance[2]
        print("Target Position of iiwa_link_ee",pos)

        self.go_to_pose_goal(pos,rot,cartesian_path)



    def TowerOfHanoi_solution(self,n,from_rod, to_rod, aux_rod):
        if n == 1:
            print("Move disk 1 from rod",from_rod,"to rod",to_rod)
            return
        self.TowerOfHanoi_solution(n-1, from_rod, aux_rod, to_rod)
        print("Move disk",n,"from rod",from_rod,"to rod",to_rod)
        self.TowerOfHanoi_solution(n-1, aux_rod, to_rod, from_rod)

    def token_printer(self,token,from_rod,to_rod):
        """ | THIS FUNCTION IS AUTOMATICALY CALLED BY THE RECURSIVE FUNCTION FOR HANOI TOWER SOLUTION.
        | This function gives command to the robot for which frame the robot should move and close or the open the gripper.

        :param token: (str) name of a token's frame
                    For example; token = "hanoi_tower_d400_frame"

        :param from_rod: (str) name of the reference frame of the rod from where each token should be taken.
                    For example; to_rod = "hanoi_board_left_top_reference_frame"

        :param to_rod: (str) name of the reference frame of the target rod to which each token should be moved.
                    For example; to_rod = "hanoi_board_right_top_reference_frame"

        """
        print("Move "+ token+ " from rod",from_rod," to rod",to_rod)
        self.go_to_frame(name_of_frame=from_rod,tolerance = [0,0,0.05])
        self.go_to_frame(name_of_frame=from_rod,tolerance = [0,0,0.01])
        self.go_to_frame(name_of_frame=token)
        self.gripper('closed')
        self.go_to_frame(name_of_frame=from_rod,tolerance = [0,0,0.05],cartesian_path=True)
        self.go_to_frame(name_of_frame=from_rod,tolerance = [0,0,0.01],cartesian_path=True)
        self.go_to_frame(name_of_frame=to_rod,tolerance = [0,0,0.05])
        self.go_to_frame(name_of_frame=to_rod,tolerance = [0,0,0.01])
        self.gripper('open')

    def TowerOfHanoi_solution_token(self,token,from_rod, to_rod, aux_rod):
        """ Hanoi Tower recursive solution function

        :param token: (list) name of the token's reference frames
                    For example; token = ['hanoi_tower_d300_frame','hanoi_tower_d400_frame','hanoi_tower_d500_frame']

        :param from_rod: (str) name of the reference frame of the rod from where each token should be taken.
                    For example; from_rod = "hanoi_board_middle_top_reference_frame"

        :param to_rod: (str) name of the reference frame of the target rod to which each token should be moved.
                    For example; to_rod = "hanoi_board_left_top_reference_frame"

        :param aux_rod: (str) name of the reference frame of the auxiliary/spare rod to which tokens can be moved temporary.
                    For example; to_rod = "hanoi_board_right_top_reference_frame"

        .. image:: media/hanoi_tower1.png

        .. image:: media/hanoi_tower2.png


        """
        if len(token) == 1:
            print("Move "+ token[0]+ " from rod",from_rod," to rod",to_rod)
            self.token_printer(token[0],from_rod,to_rod)
            return
        self.TowerOfHanoi_solution_token(token[0:len(token)-1], from_rod, aux_rod, to_rod)
        self.token_printer(token[len(token)-1],from_rod,to_rod)
        self.TowerOfHanoi_solution_token(token[0:len(token)-1], aux_rod, to_rod, from_rod)


    def hanoi_tower_pickup_test(self):
        self.gripper('open')

        my_tokens = ['hanoi_tower_d300_frame','hanoi_tower_d400_frame','hanoi_tower_d500_frame','hanoi_tower_d600_frame','hanoi_tower_d700_frame']
        self.TowerOfHanoi_solution_token(my_tokens,'hanoi_board_middle_top_reference_frame','hanoi_board_left_top_reference_frame','hanoi_board_right_top_reference_frame')
        # self.TowerOfHanoi_solution_token(my_tokens,'hanoi_board_left_top_reference_frame','hanoi_board_right_top_reference_frame','hanoi_board_middle_top_reference_frame')
        # self.go_to_frame(name_of_frame="hanoi_board_right_top_reference_frame")
        # self.go_to_frame(name_of_frame="hanoi_tower_d400_frame")
        # self.go_to_frame(name_of_frame="hanoi_board_right_top_reference_frame")

    def gripper(self,state):
        """Change gripper state. The gripper will be completely opened or closed.

        :param state: desired gripper state
        :type state: string,("open","closed")
        :return: There is no return value.
        :rtype: None
        """

        print(state + 'Gripper')
        self.move_group_gripper.set_named_target("gripper_"+ state)
        self.move_group_gripper.go()


    def get_pos_and_rot_between_frames(self,base_frame="world",target_frame="end_effector_frame"):
        """Obtaining position and orientation of target_frame with respect to base_frame.

        :param base_frame: name of base frame ( For example; "world" )
        :type state: string
        :param target_frame: name of target frame ( For example; "end_effector_frame" )
        :type state: string

        :return: It returns position vector and orientation in quaternion form.
        :rtype: location: list 1x3 (for example location= [-0.45, 0.49, 0.44])
        :rtype: rotation: list 1x4 (for example rotation= [0.01, -0.01, -0.03, 0.9])

        .. image:: media/target_frame_with_respect_to_base_frame.png
        """
        (location,rotation) = self.listener.lookupTransform(base_frame, target_frame, rospy.Time(0))
        return location,rotation

    def get_transformation_matrix(self,p,q):
        """Convert position and orientation vector into 4x4 Transformation Matrix.

        :param p: position vector
        :type p: list  ( For example; p = [-0.060, 0.6, 0.51] )
        :param q: orientation vector
        :type q: list ( For example; q =  [-1e-323, -1e-323, 0.707 0.707] )

        :return: It returns 4x4 Transformation matrix.
        :rtype: T: numpy.darray(4,4) (for example T= array([[ 1.  , -0.04, -0.01,  0.  ],
            [ 0.04,  1.  , -0.02,  0.  ],
            [ 0.01,  0.02,  1.  ,  0.  ],
            [ 0.  ,  0.  ,  0.  ,  1.  ]]))
        """
        # @Input
        # 1) parameter: p [robot location]
        #   type: list
        #   Example: p = [-0.0600164, 0.6, 0.5125]
        # 2) parameter: q [quartenion ]
        #    type : list
        #   Exampe q = [-1e-323, -1e-323, 0.7071080798594734, 0.7071054825112366]
        #Get Transformation matrix from pose and quartenions
        T = tf.transformations.quaternion_matrix([q[0],q[1],q[2],q[3]])
        #adding positions to the positon part of the transformation matrix
        T[:3,3] = np.array([p[0],p[1],p[2]])
        return T
    def Rodrigues(self,angle,axis):
        """Rodrigues rotation matrix

        :param angle: rotation angle in radian
        :type angle:  float (For example; angle = math.pi/2)
        :param axis: 1x3 unit vector [x,y,z] in Cartesian coordinate system
        :type axis: list ( For example; axis = [1,1,1]/sqrt(3) )
        :return: It returns 4x4 Matrix
        :rtype: numpy.ndarray (4,4)

        .. image:: media/rod.png

        """
        #Rotation matrix 4x4 format
        # Inputs @Angle double in radian
        #        @Axis is tuple for example for z axis (0, 0, 1)
        rotation_matrix = tf.transformations.rotation_matrix(angle,axis)
        return rotation_matrix

    def Rx(self,angle):
        """Rotation matrix about x axis

        :param angle: (float) rotation angle in radian
                    For example; angle = math.pi/2

        .. image:: media/Rx.png

        """
        #Rotation about x axis by angle
        xaxis = (1, 0, 0)
        return self.Rodrigues(angle,xaxis)

    def Ry(self,angle):
        """Rotation matrix about y axis

        :param angle: (float) rotation angle in radian
                    For example; angle = math.pi/2

        .. image:: media/Ry.png

        """
        #Rotation about y axis by angle
        yaxis = (0, 1, 0)
        return self.Rodrigues(angle,yaxis)

    def Rz(self,angle):
        """Rotation matrix about z axis

        :param angle: (float) rotation angle in radian
                    For example; angle = math.pi/2

        .. image:: media/Rz.png

        """
        #Rotation about z axis by angle
        zaxis = (0, 0, 1)
        return self.Rodrigues(angle,zaxis)



def main():
    moveit_kuka = HanoiTowerPickup(namespace="/iiwa", robot_description="/iiwa/robot_description")
    rospy.loginfo("MoveIt Commander is initialized...")
    moveit_kuka.get_basic_information()
    moveit_kuka.joint_command_test()
    # moveit_kuka.go_to_pose_goal()
    # moveit_kuka.add_and_attach_mesh()
    # moveit_kuka.go_to_frame()
    # moveit_kuka.hanoi_tower_pickup_test()
    # from_rod = "hanoi_board_middle_top_reference_frame"
    # token = "hanoi_tower_d700_frame"
    # to_rod = "hanoi_board_left_top_reference_frame"    
    # moveit_kuka.go_to_frame(name_of_frame=from_rod,tolerance = [0,0,0.05])
    # moveit_kuka.go_to_frame(name_of_frame=from_rod,tolerance = [0,0,0.01])
    # moveit_kuka.go_to_frame(name_of_frame=token)
    # moveit_kuka.gripper('closed')
    # moveit_kuka.go_to_frame(name_of_frame=from_rod,tolerance = [0,0,0.05],cartesian_path=True)
    # moveit_kuka.go_to_frame(name_of_frame=from_rod,tolerance = [0,0,0.01],cartesian_path=True)
    # moveit_kuka.go_to_frame(name_of_frame=to_rod,tolerance = [0,0,0.05])
    # moveit_kuka.go_to_frame(name_of_frame=to_rod,tolerance = [0,0,0.01])
    # moveit_kuka.gripper('open')
    # moveit_kuka.gripper('closed')
    # moveit_kuka.gripper('open')
    # moveit_kuka.go_to_frame(name_of_frame=token)
    # plan, fraction = moveit_kuka.plan_cartesian_path(scale=1)
    # moveit_kuka.display_trajectory(plan)
    # rospy.sleep(1)
    #moveit_commander.roscpp_shutdown()

if __name__ == '__main__':
    main()
